const {ipcRenderer, electronSend} = require('electron'),
	{ WebClient } = require('@slack/client'),
	Store = require('electron-store'),
	store = new Store();

document.addEventListener('DOMContentLoaded', () => {
	// variable declaration
	let myHasToken = store.has('userToken'), myToken = undefined,
		myNotification = undefined,
		myHasUnread = false,
		myWebClient = undefined,
		myChannelsArray= [],
		myTokenInput
	
	/**
	 * If user has set a token before, I count unread notifications,
	 *  else I show input for how to set token
	 */
	if (myHasToken) {
		
		//Token registered by the user
		myToken = store.get('userToken')
		
		// I hide the token set form
		document.getElementById('formSetTokenId').setAttribute('hidden', '')
		
		// I send the proper notification
		myNotification = new Notification('App\'s Working!', {
			body: 'Nice work. Token is set. '
		})
		
		// Tell the notification to show the top bar popup window on click
		myNotification.onclick = () => { ipcRenderer.send('show-window') }
		// Initialize the webclient with the passed token
		try {
			
			myWebClient = new WebClient(myToken)
		} catch (e) {
			console.log('Connection with token failed \n ' + e.toString())
			alert('Connection with token failed \n ' + e.toString())
			
			// reveal the input form
			document.getElementById('formSetTokenId').removeAttribute('hidden')
			
			//remove previous token value
			store.delete('token')
			
			//reload view
			ipcRenderer.reload(true)
		}
		
		// list all (first 100) conversations
		myWebClient.conversations.list({
			limit: 100,
			exclude_archived: true,
			types: 'public_channel, private_channel, mpim, im'
		})
		.then( function (pResponseData) {
			
			myChannelsArray = []
			myChannelsArray = pResponseData.channels
			
			// Cycle the array of slack channels
			myChannelsArray.forEach(function (pResponseData) {
				
				// get info of current conversation channel
				myWebClient.conversations.info({ channel: pResponseData.id})
				.then(function (pChannelData) {
					
					// checking if is channel has unread messages or not
					if (pChannelData.channel.unread_count > 0 ) {
						
						// write channel name in container
						document.getElementById('codeID')
							.innerHTML += pResponseData.id + ' in ' + pChannelData.channel.name + '<br>'
						
						myHasUnread = true;
					}
				})
			})
			
		})
		.then(function () {
			// display message for no unread schannel
			if (!hasUnread) {
				document.getElementById('codeID')
					.innerHTML += "<h6>You don't have any unread element</h6> " + '<br><br>'
			}
		})
	} else  {
		
		// notification creation
		myNotification = new Notification('Please configure your slack!', {
			body: 'Click on this notification to open the popup where to insert your data'
		})
		
		// Tell the notification to show the menubar popup window on click
		myNotification.onclick = () => { ipcRenderer.send('show-window') }
	}
	
	document.querySelector('#tokenFormSubmit').addEventListener('click', function() {
		
		// Retrieve input from the field
		myTokenInput = document.getElementById("tokenInput").value;
		
		// send token to main.js
		ipcRenderer.send('asynchronous-message', {'token' : myTokenInput} )
		
		// receive message from main.js
		ipcRenderer.on('asynchronous-reply', (event, arg) => {
			ipcRenderer.reload(true)
		})
		
	})
	
})

